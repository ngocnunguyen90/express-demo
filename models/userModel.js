'use strict';
class User {
    constructor(id, password) {
        // always initialize all instance properties
        this.id = id;
        this.password = password;
    }
    getUsername() {
        return this.id;
    }
    getUserPassword() {
        return this.password;
    }
};

module.exports = User;
