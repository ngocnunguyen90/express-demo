var express = require('express');
var router = express.Router();

var loginController = require('../controllers/loginController.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('users');
});

router.post('/login', loginController.login_detail);

router.get('/list', loginController.login_list);

module.exports = router;
